# Happy Brewery

A simple web application for the Happy Brewery and placement of their products, along with the back
office.

## Implementation

Jersey and Hibernate are used for the REST resources, while the static HTML, JavaScript, and CSS
pages stand alone. The Java web application is in _backend_ folder, while the static documents are in
_frontend_ folder. Nginx is used to make both seem a part of the same application.

The _backend_ folder is a maven project which consists of two modules, _core_ and _rest_. _core_
contains all the back logic and classes which are persisted in the database. _rest_ contains 
RESTful web services consumed by the frontend.

