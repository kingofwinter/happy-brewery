package com.happy.brewery.persistence.dao;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

/**
 * {@link EntityManager} provider which holds an instance of the entity manager for each thread.
 * 
 * @author kingofwinter
 *
 */
public class EMProvider {
	
	/**
	 * Collection of managers
	 */
	private static ThreadLocal<EntityManager> managers = new ThreadLocal<>();

	/**
	 * Retrieve an {@link EntityManager} for this thread.
	 * 
	 * @return entity manager
	 */
	public static EntityManager getEntityManager() {
		EntityManager manager = managers.get();
		
		if (manager == null) {
			manager = EMFactoryProvider.getEntityManagerFactory().createEntityManager();
			managers.set(manager);
		}
		
		return manager;
	}
	
	/**
	 * Closes this thread's entity manager.
	 * 
	 * @throws DAOException if the manager's last transaction cannot be committed
	 * or the manager cannot be closed
	 */
	public static void close() {
		EntityManager manager = managers.get();
		if (manager == null) return;
		
		DAOException dex = null;
		try {
			if (manager.getTransaction().isActive()) {
				manager.getTransaction().commit();
			}
		} catch (RollbackException e) {
			dex = new DAOException("Cannot commit last transaction", e);
		}
		
		try {
			manager.close();
		} catch (IllegalStateException e) {
			if (dex == null) {
				dex = new DAOException("Cannot close entity manager", e);
			}
		}
		
		managers.remove();
		if (dex != null) throw dex;
	}
}
