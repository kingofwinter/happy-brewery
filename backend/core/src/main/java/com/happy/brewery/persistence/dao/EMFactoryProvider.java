package com.happy.brewery.persistence.dao;

import javax.persistence.EntityManagerFactory;

/**
 * {@link EntityManagerFactory} provider which holds a reference to the factory, effectively
 * making it a <code>Singleton</code>.
 * 
 * 
 * @author kingofwinter
 *
 */
public class EMFactoryProvider {
	
	/**
	 * Reference to the factory
	 */
	private static EntityManagerFactory emf;
	
	/**
	 * Set the <code>EntityManagerFactory</code> once it is created
	 * 
	 * @param newEmf the factory
	 */
	public static void setEntityManagerFactory(EntityManagerFactory newEmf) {
		emf = newEmf;
	}
	
	/**
	 * Retrieve the <code>EntityManagerFactory</code>
	 * 
	 * @return the factory
	 */
	public static EntityManagerFactory getEntityManagerFactory() {
		return emf;
	}

}
