package com.happy.brewery.persistence.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for data access objects with minimal functionalities.
 * 
 * @author kingofwinter
 *
 * @param <T> entity class
 */
public interface DAO<T extends Serializable> {

	/**
	 * Retrieve all entity records from the table. It is equivalent to
	 * <br>
	 * <code> SELECT * FROM table_name_of_T </code>
	 * <br>
	 * statement in SQL
	 * 
	 * @return all stored entities
	 */
	List<T> getAll();
	
	/**
	 * Retrieve an entity record by its identifier.
	 * 
	 * @param id entity's identifier
	 * 
	 * @return the entity
	 */
	T get(long id);
	
	/**
	 * Persist an entity.
	 * 
	 * @param entity
	 */
	void create(T entity);
	
	/**
	 * Update entity record's values
	 * 
	 * @param entity
	 * 
	 * @return updated entity
	 */
	T update(T entity);
	
	/**
	 * Delete an entity record
	 * 
	 * @param entity
	 */
	void delete(T entity);
}
