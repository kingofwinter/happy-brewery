package com.happy.brewery.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * A currency represented by it's {@code code}, and {@code country} it is used in.
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name = "currencies")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class Currency implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -4301362126660628412L;

	/**
	 * Identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Name of the country/area in which this currency is used
	 */
	@Column(nullable = false, length = 40)
	private String country;
	
	/**
	 * Code of the currency, such as {@code EUR}, or {@code USD}
	 */
	@Column(nullable = false, length = 3)
	private String code;
	
	public Currency() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
