package com.happy.brewery.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * A purchase of an ingredient.
 * <p>
 * Once an ingredient is purchased, its price and currency are logged, along with the amount purchased.
 * A database procedure should be created in order to calculate total quantity of the ingredient once
 * a purchase is logged.
 * </p>
 * 
 * @see IngredientStock
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name = "ingredient_purchases")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class IngredientPurchase implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -5306534591425503447L;

	/**
	 * Identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Reference to the ingredient
	 */
	@ManyToOne
	private IngredientStock ingredient;
	
	/**
	 * Reference to the currency used in the purchase
	 */
	@ManyToOne
	private Currency currency;
	
	/**
	 * Price of the purchase
	 */
	@Column(nullable = false, precision = 5, scale = 2)
	private double price;
	
	/**
	 * Amount of the ingredient purchased, measurement unit being in the {@link IngredientStock}
	 */
	@Column(nullable = false, precision = 5, scale = 2)
	private double amount;
	
	public IngredientPurchase() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IngredientStock getIngredient() {
		return ingredient;
	}

	public void setIngredient(IngredientStock ingredient) {
		this.ingredient = ingredient;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
