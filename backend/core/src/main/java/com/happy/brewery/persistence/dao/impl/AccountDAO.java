package com.happy.brewery.persistence.dao.impl;

import java.util.List;

import com.happy.brewery.persistence.dao.DAOException;
import com.happy.brewery.persistence.dao.EMProvider;
import com.happy.brewery.persistence.dao.SimpleDAO;
import com.happy.brewery.persistence.entities.Account;
import com.happy.brewery.persistence.util.DBUtil;

/**
 * Implementation of <code>DAO</code> for {@link Account}.
 * <p>
 * Due to safety reasons, the {@link #getAll()} is implemented in such a way that all the accounts' passwords are set to null.
 * Again, due to safety reasons, each password should be encrypted before persistence.
 * </p>
 * <p>
 * In order to prevent using same username for multiple accounts, {@link #create(Account)} checks whether the new entity's
 * username is already in use.
 * </p>
 * 
 * @author kingofwinter
 *
 */
public class AccountDAO extends SimpleDAO<Account> {

	@Override
	public List<Account> getAll() {
		List<Account> results = EMProvider.getEntityManager().createQuery("SELECT a FROM Account a", Account.class).getResultList();
		results.forEach(acc -> acc.setPassword(null));
		return results;
	}

	@Override
	public void create(Account entity) {
		if (DBUtil.usernameExists(entity.getUsername())) {
			throw new DAOException("username already in use: " + entity.getUsername());
		}
		super.create(entity);
	}
	
}
