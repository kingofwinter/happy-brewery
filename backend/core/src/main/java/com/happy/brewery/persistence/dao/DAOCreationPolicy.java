package com.happy.brewery.persistence.dao;

/**
 * Policies for creation of the object's <code>DAO</code>.
 * 
 * @author kingofwinter
 *
 */
public enum DAOCreationPolicy {

	/**
	 * Create {@link SimpleDAO}
	 */
	AUTO,
	
	/**
	 * Use custom <code>DAO</code> object
	 */
	CUSTOM
}
