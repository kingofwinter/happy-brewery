package com.happy.brewery.persistence.dao;

/**
 * Exception which is generally thrown when JPA and DAO operations and
 * transactions fail.
 * 
 * @author kingofwinter
 *
 */
public class DAOException extends RuntimeException {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -6016573834753078070L;

	/**
	 * Default constructor.
	 */
	public DAOException() {
		super();
	}

	/**
	 * Initialises the exception with its message and cause, and passes
	 * it to superclass's constructor
	 * 
	 * @param message description of the case which caused the exception
	 * @param cause exception caught
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Initialises the exception with its message and passes it to superclass's constructor.
	 * 
	 * @param message description of the case which caused the exception
	 */
	public DAOException(String message) {
		super(message);
	}

	/**
	 * Initialises the exception with its cause
	 * 
	 * @param cause caught exception
	 */
	public DAOException(Throwable cause) {
		super(cause);
	}

	
}
