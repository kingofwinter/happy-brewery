package com.happy.brewery.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * Represents a single ingredient used for brewing beer.
 * <p>
 * An ingredient can be used in multiple recipes, like the common ones:
 * <ul>
 * 	<li>Malt</li>
 * 	<li>Hop</li>
 * 	<li>Barley</li>
 * 	<li>etc.</li>
 * </ul>
 * Each of them is represented by a single entry of this class.
 * </p>
 * <p>
 * Each ingredient has a default measurement unit, e.g. {@code kg} or {@code l}. The exact unit needs to be
 * chosen wisely when initiating an ingredient, since all the ingredient's usages in recipes and purchases
 * will use said unit. The calculation between units, e.g. {@code kg} to {@code g}, is not yet supported.
 * </p>
 * <p>
 * Each ingredient has total quantity calculated by a stored procedure in the database which is triggered
 * whenever a quantity of the ingredient is purchased.
 * </p>
 * 
 * @see IngredientPurchase
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name = "ingredient_names")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class IngredientStock implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 5162356731269073640L;

	/**
	 * Identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Name of the ingredient, such as {@code Barley}
	 */
	@Column(nullable = false, length = 50)
	private String name;

	/**
	 * Measurement of the ingredient, e.g. 5.2 <b>kg</b> wheat
	 */
	@Column(nullable = false, length = 5)
	private String measurement;
	
	/**
	 * Total quantity of the ingredient 
	 */
	@Column(nullable = false, precision = 5, scale = 2)
	private Double totalQuantity;
	
	/**
	 * Default constructor, used by JPA
	 */
	public IngredientStock() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
