package com.happy.brewery.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;
import com.happy.brewery.persistence.util.DBUtil;

/**
 * Simple account assigned to a user which can use the back office.
 * <p>
 * Once a {@link User} is wished to use the application autonomously in any manner, they are given an account
 * protected by a user name and password. JPA does not specify any level of encryption, so the encryption must
 * be done using a stored procedure in the database itself.
 * </p>
 * <p>
 * Each account has a set of permissions for using various functionalities of the application:
 * <ul>
 * <li>Brewing - account can manage recipes and create batches</li>
 * <li>Consuming - account can order bottles</li>
 * <li>Managing users - account can add new users and assign their accounts</li>
 * <li>Supply - account can make purchases, update ingredient stocks, and view expenses</li>
 * <li>Permission managing - account can pass down its permissions to other users</li>
 * </ul>
 * Initial user is also a super-user with all the permissions, which he can later pass down to other users.
 * Default user is a simple consumer which can view batches and order bottles.
 * </p>
 * 
 * @author kingofwinter
 *
 */
@NamedQueries({
	@NamedQuery(name = "account.selectByUsername",
			query = "SELECT a FROM Account a WHERE a.username = :username")
})
@Entity
@Table(name = "accounts")
@CreateDAO(policy = DAOCreationPolicy.CUSTOM, name = "com.happy.brewery.persistence.dao.impl.AccountDAO")
public class Account implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 7878165101774603252L;

	/**
	 * Entity identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Reference to the user
	 */
	@OneToOne
	private User user;
	
	/**
	 * Account's user name
	 */
	@Column(nullable = false)
	private String username;
	
	/**
	 * Account's password
	 */
	@Column(nullable = false, length = DBUtil.PASS_LEN)
	private String password;
	
	/**
	 * Indicates whether account can manage recipes and create batches
	 */
	@Column(name = "brewer")
	private boolean isBrewer = false;
	
	/**
	 * Indicates whether account can view batches and make orders
	 */
	@Column(name = "consumer")
	private boolean isConsumer = true;
	
	/**
	 * Indicates whether account can add new users and assign accounts
	 */
	@Column(name = "human_res")
	private boolean isHumanRes = false;
	
	/**
	 * Indicates whether account can make purchases and view overall expenses
	 */
	@Column(name = "supplier")
	private boolean isSupplier = false;
	
	/**
	 * Indicates whether account can assign their own permissions to other accounts
	 */
	@Column(name = "forward_permissions")
	private boolean canForwardPermissions = false;
	
	public Account() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isBrewer() {
		return isBrewer;
	}

	public void setBrewer(boolean isBrewer) {
		this.isBrewer = isBrewer;
	}

	public boolean isConsumer() {
		return isConsumer;
	}

	public void setConsumer(boolean isConsumer) {
		this.isConsumer = isConsumer;
	}

	public boolean isHumanRes() {
		return isHumanRes;
	}

	public void setHumanRes(boolean isHumanRes) {
		this.isHumanRes = isHumanRes;
	}

	public boolean isSupplier() {
		return isSupplier;
	}

	public void setSupplier(boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public boolean canForwardPermissions() {
		return canForwardPermissions;
	}

	public void setCanForwardPermissions(boolean canForwardPermissions) {
		this.canForwardPermissions = canForwardPermissions;
	}
	
	
}
