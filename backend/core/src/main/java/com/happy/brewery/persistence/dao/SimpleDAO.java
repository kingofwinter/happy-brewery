package com.happy.brewery.persistence.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Simple implementation of {@link DAO}.
 * <p>
 * The class offers implementation of basic CRUD operations, but should be extended by 
 * a <code>DAO</code> with entity-specific methods.
 * </p>
 * 
 * @author kingofwinter
 *
 * @param <T> entity
 */
public abstract class SimpleDAO<T extends Serializable> implements DAO<T> {

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() {
		return EMProvider
				.getEntityManager()
				.createQuery("SELECT e FROM " + getT().getName() + " e")
				.getResultList();
	}

	@Override
	public T get(long id) {
		return EMProvider.getEntityManager().find(getT(), id);
	}

	@Override
	public void create(T entity) {
		EMProvider.getEntityManager().persist(entity);
	}

	@Override
	public T update(T entity) {
		return EMProvider.getEntityManager().merge(entity);
	}

	@Override
	public void delete(T entity) {
		EMProvider.getEntityManager().remove(entity);
	}

	@SuppressWarnings("unchecked")
	private Class<T> getT() {
		return (Class<T>) ((ParameterizedType) getClass().
				getGenericSuperclass()).
				getActualTypeArguments()[0];
	}
}
