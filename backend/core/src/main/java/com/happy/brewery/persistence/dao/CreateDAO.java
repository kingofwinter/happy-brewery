/**
 * 
 */
package com.happy.brewery.persistence.dao;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation used for indication that {@link DAOProvider} should provide a {@link DAO} for the annotated class.
 * <p>
 * {@link #policy()} defines whether the <code>DAO</code> will be automatically generated {@link SimpleDAO}, or
 * a custom class defined by <code>name</code>.
 * {@link #name()} defines the fully qualified name of the class' <code>DAO</code>.
 * </p>
 * 
 * @author kingofwinter
 *
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface CreateDAO {

	DAOCreationPolicy policy();
	
	String name() default "";
}
