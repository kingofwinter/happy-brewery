package com.happy.brewery.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * Simple user.
 * <p>
 * A user is created by the master brewer in order to track who is ordering and consuming bottles.
 * </p>
 * <p>
 * User can be made into an {@link Account} in order to give him permissions to operate the brewery.
 * </p>
 * 
 * @see Account
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name = "users")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class User implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 3533486714213937786L;

	/**
	 * Entity identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * User's first name
	 */
	@Column(nullable = false)
	private String name;
	
	/**
	 * User's last name
	 */
	@Column(nullable = false)
	private String surname;
	
	public User() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	
}
