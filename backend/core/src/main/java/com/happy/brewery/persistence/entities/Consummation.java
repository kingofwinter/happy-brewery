package com.happy.brewery.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * A user's consummation of beer expressed in bottles.
 * <p>
 * A user can be given a number of bottles from a batch of beer.
 * </p>
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name = "consummations")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class Consummation implements Serializable {
	
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -505811096812419491L;

	/**
	 * Entity identifier
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Reference to the user who made an order
	 */
	@ManyToOne
	private User user;
	
	/**
	 * Number of bottles ordered
	 */
	@Column
	private int bottles;
	
	/**
	 * Time at which the order was created
	 */
	@Temporal(TemporalType.TIME)
	private Date time;
	
	public Consummation() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getBottles() {
		return bottles;
	}

	public void setBottles(int bottles) {
		this.bottles = bottles;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	
}
