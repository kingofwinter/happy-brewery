package com.happy.brewery.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * A batch of beer brewed using a {@link Recipe}.
 * <p>
 * A recipe can be used to create batches of beer, each with information on the final yield
 * and number of bottles. Number of bottles left should be managed by a stored procedure in the database
 * which removes bottles whenever an order on the bottles is placed.
 * </p>
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name = "batches")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class Batch implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -4078305311618056913L;

	/**
	 * Identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Recipe for this particular batch of beer
	 */
	@ManyToOne
	private Recipe recipe;
	
	/**
	 * Total yield of the batch
	 */
	@Column(name = "final_yield", precision = 5, scale = 2)
	private double finalYield;
	
	/**
	 * Number of bottles produced
	 */
	@Column(name = "init_bottles")
	private int initialBottles;
	
	/**
	 * Number of bottles left
	 */
	@Column(name = "left_bottles")
	private int bottlesLeft;
	
	/**
	 * Point at which the batch was created
	 */
	@Temporal(TemporalType.TIME)
	private Date created;
	
	public Batch() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public double getFinalYield() {
		return finalYield;
	}

	public void setFinalYield(double finalYield) {
		this.finalYield = finalYield;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public int getInitialBottles() {
		return initialBottles;
	}

	public void setInitialBottles(int initialBottles) {
		this.initialBottles = initialBottles;
	}

	public int getBottlesLeft() {
		return bottlesLeft;
	}

	public void setBottlesLeft(int bottlesLeft) {
		this.bottlesLeft = bottlesLeft;
	}

}
