package com.happy.brewery.persistence.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * A beer recipe.
 * <p>
 * A recipe consists of the list of ingredients and instructions on how to use them. Future versions will have an implementation
 * of the list of steps, along with the hopping process.
 * <p>
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name="recipes")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class Recipe implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -4157257616004929441L;

	/**
	 * Identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * List of ingredients needed for the recipe
	 */
	@OneToMany(mappedBy = "recipe")
	private List<RecipeIngredient> ingredients;
	
	/**
	 * Instructions to how to brew the beer
	 */
	@Column(length = 1000)
	private String instructions;
	
	/**
	 * JSON containing instructions for the hopping process
	 */
	@Column(length = 1500)
	private String hopping;
	
	/**
	 * Yield of a round of brewing
	 */
	@Column(nullable = false, precision = 5, scale = 2)
	private double yield;
	
	public Recipe() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RecipeIngredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<RecipeIngredient> ingredients) {
		this.ingredients = ingredients;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public double getYield() {
		return yield;
	}

	public void setYield(double yield) {
		this.yield = yield;
	}

	public String getHopping() {
		return hopping;
	}

	public void setHopping(String hopping) {
		this.hopping = hopping;
	}
}
