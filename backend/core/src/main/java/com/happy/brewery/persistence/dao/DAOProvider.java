package com.happy.brewery.persistence.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Provider of <code>DAO</code>s for classes annotated with {@link CreateDAO}.
 * <p>
 * Underlying mechanism returns {@link SimpleDAO} implementation for given class if its {@link DAOCreationPolicy}
 * is <code>AUTO</code>, or instantiates the one declared in the annotation.
 * </p>
 * <p>
 * If used in a container with multiple class loaders, such as a web application container, 
 * the provider should be cleared once the application is being destroyed, in order to prevent data leakage.
 * </p>
 * 
 * @author kingofwinter
 *
 */
public class DAOProvider {
	
	/**
	 * Map of DAOs
	 */
	@SuppressWarnings("rawtypes")
	private static Map<Class, DAO> daos = new HashMap<>();

	/**
	 * Retrieve the {@link DAO} for class <code>T</code>.
	 * 
	 * @param <T> a class annotated with {@link CreateDAO}
	 * @param forClass class for which the <code>DAO</code> is created
	 * 
	 * @return <code>DAO</code>
	 * 
	 * @throws NullPointerException if <code>forClass</code> is a <code>null</code> reference,
	 * or the provider has already been cleared
	 * @throws DAOException if <code>T</code> is not annotated with <code>@CreateDAO</code>, or
	 * the custom <code>DAO</code> specified in the annotation doesn't exist
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> DAO<T> get(Class<T> forClass) {
		Objects.requireNonNull(forClass);
		DAO<T> result = daos.get(forClass);
		if (result == null) {
			result = createDAO(forClass);
		}
		
		return result;
	}
	
	/**
	 * Creates a <code>DAO</code> and stores it into {@link #daos}.
	 * 
	 * @param <T> class annotated with <code>@CreateDAO</code>
	 * @param forClass class for which the <code>DAO</code> is created
	 * 
	 * @return <code>DAO</code>
	 * 
	 * @throws DAOException if <code>T</code> is not annotated with <code>@CreateDAO</code>, or
	 * the custom <code>DAO</code> specified in the annotation doesn't exist
	 */
	@SuppressWarnings("unchecked")
	private static <T extends Serializable> DAO<T> createDAO(Class<T> forClass) {
		CreateDAO annotation = forClass.getAnnotation(CreateDAO.class);
		if (annotation == null) {
			throw new DAOException(String.format("Cannot create a DAO for unknown class: %s", forClass.getName()));
		}
		
		DAO<T> result = null;
		if (annotation.policy() == DAOCreationPolicy.AUTO) {
			result = new SimpleDAO<T>() {};
			daos.put(forClass, result);
			return result;
		}
		
		try {
			result = (DAO<T>) Class.forName(annotation.name()).getConstructor().newInstance();
			daos.put(forClass, result);
		} catch (Exception e) {
			throw new DAOException("Cannot instantiate " + annotation.name(), e);
		}
		
		return result;
	}
	
	/**
	 * Clear the map containing the references to the classes and DAOs in order
	 * to prevent any data leakage.
	 * 
	 * @throws NullPointerException if the map has already been cleared
	 */
	public static void clear() {
		daos.clear();
		daos = null;
	}
}
