package com.happy.brewery.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * A single ingredient in a recipe.
 * <p>
 * Ingredient in a recipe is given by its {@link #quantity} (e.g. 5), {@link #measurement} (e.g. l), and {@link #name} (e.g. water).
 * The ingredient is usually a part of the list of ingredients in a recipe.
 * </p>
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name = "ingredients")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class RecipeIngredient implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -2956925395754854170L;

	/**
	 * Identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Quantity of the ingredient, e.g. <b>5.2</b> kg wheat
	 */
	@Column(nullable = false, precision = 5, scale = 2)
	private double quantity;
	
	/**
	 * Name of the ingredient mapped by {@link IngredientStock}, e.g. 5.2 kg <b>wheat</b>
	 */
	@ManyToOne
	private IngredientStock name;
	
	/**
	 * Reference to the owner of the ingredient
	 */
	@ManyToOne
	private Recipe recipe;
	
	/**
	 * Default constructor, used by JPA
	 */
	public RecipeIngredient() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public IngredientStock getName() {
		return name;
	}

	public void setName(IngredientStock name) {
		this.name = name;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
	
}
