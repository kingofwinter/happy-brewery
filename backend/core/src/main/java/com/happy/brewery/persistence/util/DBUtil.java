package com.happy.brewery.persistence.util;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

import org.bouncycastle.crypto.generators.SCrypt;

import com.happy.brewery.persistence.dao.EMProvider;
import com.happy.brewery.persistence.entities.Account;

/**
 * Class with utility methods for account and password handling.
 * 
 * @author kingofwinter
 *
 */
public class DBUtil {
	
	/**
	 * Salt used for password generation
	 */
	private static final String SALT = "hd283c0uha0u8hrcnh";

	/**
	 * Memory cost parameter used for password generation
	 */
	private static final int COST = 8;
	
	/**
	 * Block size parameter used for password generation
	 */
	private static final int BLOCK_SIZE = 8;
	
	/**
	 * Parallelisation parameter used for password generation
	 */
	private static final int PARALLEL_PARAM = 8;
	
	/**
	 * Length of the generated password
	 */
	public static final int PASS_LEN = 64;
	
	/**
	 * Key derivation function for encryption of passwords.
	 * 
	 * @param plain password stored in plain text
	 * 
	 * @return encrypted password
	 * 
	 * @throws NullPointerException if <code>plain</code> is a null reference
	 */
	public static String generatePassword(String plain) {
		Objects.requireNonNull(plain);
		return new String(
				SCrypt.generate(
						plain.getBytes(StandardCharsets.UTF_8), 
						SALT.getBytes(StandardCharsets.UTF_8),
						COST,
						BLOCK_SIZE,
						PARALLEL_PARAM,
						PASS_LEN),
				StandardCharsets.UTF_8);
	}
	
	/**
	 * Retrieves an account by username.
	 * 
	 * @param username username
	 * @return account if exists, <code>null</code> otherwise
	 */
	public static Account getAccountByUsername(String username) {
		List<Account> accounts = getAllAccountsByUsername(username);
		if (accounts.size() == 0) {
			return null;
		}
		
		return accounts.get(0);
	}
	
	/**
	 * Checks whether an account with given <code>username</code> already exists in the database.
	 * 
	 * @param username username
	 * @return <code>true</code> if the username is already in use, <code>false</code> otherwise
	 */
	public static boolean usernameExists(String username) {
		return getAllAccountsByUsername(username).size() != 0;
	}
	
	/**
	 * Retrieves all accounts with given <code>username</code>
	 * 
	 * @param username username
	 * 
	 * @return list of accounts
	 */
	private static List<Account> getAllAccountsByUsername(String username) {
		return EMProvider.getEntityManager().createNamedQuery("account.selectByUsername", Account.class).getResultList();
	}
}
