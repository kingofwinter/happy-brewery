package com.happy.brewery.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.happy.brewery.persistence.dao.CreateDAO;
import com.happy.brewery.persistence.dao.DAOCreationPolicy;

/**
 * Denotes the style of the beer.
 * <p>
 * Beers are divided by styles, such as {@code Lager}, {@code India pale ale}, {@code Stout}, and so on.
 * A style of beer is represented by its {@link #name}.
 * </p>
 * 
 * @author kingofwinter
 *
 */
@Entity
@Table(name="styles")
@CreateDAO(policy = DAOCreationPolicy.AUTO)
public class Style implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -4001726880207457362L;

	/**
	 * Identifier
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Name of the style
	 */
	@Column(nullable=false, length=30)
	private String name;

	/**
	 * Default constructor, used by the JPA.
	 */
	public Style() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
