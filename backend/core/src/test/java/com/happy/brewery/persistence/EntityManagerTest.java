package com.happy.brewery.persistence;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.Test;

import com.happy.brewery.persistence.dao.EMFactoryProvider;
import com.happy.brewery.persistence.dao.EMProvider;

public class EntityManagerTest {
	
	@Test
	public void testEMF() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.happy.brewery.jpa");
		EMFactoryProvider.setEntityManagerFactory(emf);
		
		assertNotNull(EMProvider.getEntityManager());
	}
}