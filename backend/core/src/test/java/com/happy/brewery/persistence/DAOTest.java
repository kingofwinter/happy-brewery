package com.happy.brewery.persistence;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.happy.brewery.persistence.dao.DAO;
import com.happy.brewery.persistence.dao.DAOProvider;
import com.happy.brewery.persistence.dao.SimpleDAO;
import com.happy.brewery.persistence.dao.impl.AccountDAO;
import com.happy.brewery.persistence.entities.Account;
import com.happy.brewery.persistence.entities.Batch;
import com.happy.brewery.persistence.entities.Consummation;
import com.happy.brewery.persistence.entities.User;
import com.happy.brewery.persistence.util.DBUtil;

public class DAOTest {
	
	@Test
	public void testDAOProvider() {
		assertTrue(DAOProvider.get(Batch.class) instanceof DAO<?>);
		assertTrue(DAOProvider.get(Consummation.class) instanceof SimpleDAO<?>);
		assertTrue(DAOProvider.get(Account.class) instanceof AccountDAO);
	}

	public void testAccountDAO() {
		User user = new User();
		user.setName("Domagoj");
		user.setSurname("Maslovar");
		Account acc = new Account();
		acc.setUser(user);
		acc.setUsername("maslinator");
		acc.setPassword(DBUtil.generatePassword("aPassword"));
		
		DAOProvider.get(Account.class).create(acc);
		assertTrue(acc.getId() != null);
		
		Account persisted = DAOProvider.get(Account.class).get(acc.getId());
		assertTrue(acc.getId().equals(persisted.getId()));
		
		assertTrue(acc.getUsername().equals(persisted.getUsername()));
	}
}
